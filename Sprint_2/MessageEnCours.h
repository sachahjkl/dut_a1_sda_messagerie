#ifndef _MESSAGE_EN_COURS_
#define _MESSAGE_EN_COURS_


/**
* @file MessageEnCours.h
* Projet Sprint 2
* @author Abelhaj Youssef et Froment Sacha G106
* @version 1 - 14/12/2017
* @brief Composant  de message en cours
* Structures de donn�es et algorithmes - DUT1 Paris Descartes
*/

/**
* @brief Structure de donn�es de type MessageEnCours
*/
#include "PaquetReseau.h"
struct  MessageEnCours{
	enum { MAX = 21 };
	IdMessage IdMess;
	unsigned int LgMes;
	unsigned int nbPRecus;
	unsigned int LastPRecu;
};

/** Initialiser et allouer en m�moire dynamique
la variable m de type MessageEnCours */
void initialiser_mc(MessageEnCours& mc, const PaquetReseau& pr);


/*FONCTION "DETRUIRE" INUTILE POUR L'INSTANT*/

/** D�sallouer la variable m de type MessageEnCours */
/*void detruire_mc(MessageEnCours& mc);*/

#endif