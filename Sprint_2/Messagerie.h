#ifndef _MESSAGERIE_
#define _MESSAGERIE_

/**
* @file Messagerie.h
* Projet Sprint 2
* @author Abelhaj Youssef et Froment Sacha G106
* @version 1 - 14/12/2017
* @brief Composant de la messagerie
* Structures de donn�es et algorithmes - DUT1 Paris Descartes
*/

#include "PaquetReseau.h"
#include "Liste.h"

/**
* @brief Structure de donn�es de type Messagerie
*/

struct  Messagerie{
	enum { MAX = 21 };
	Liste listeM;
};

/** allouer et d�sallouer la variable m de type Messagerie */
void initialiser(Messagerie& m);
void detruire(Messagerie& m);

/** @brief Reception d'un paquet-r�seau */
void recevoirPaquetReseau(std::istream& is, PaquetReseau& pr);

/** @brief Organiser un paquet-r�seau dans la messagerie */
void traiterPaquetReseau(Messagerie& m, PaquetReseau& pr);

/** @brief Affichage d'un paquet-r�seau */
void afficher_mess(std::ostream& os, Messagerie& m, const PaquetReseau& pr);

#endif