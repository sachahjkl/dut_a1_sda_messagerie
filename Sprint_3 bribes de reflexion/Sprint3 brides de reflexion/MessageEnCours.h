#ifndef _MESSAGERIE_
#define _MESSAGERIE_


/**
* @file MessageEnCours.h
* Projet Sprint 1
* @author Abelhaj Youssef et Froment Sacha G106
* @version 1 - 14/12/2017
* @brief Composant du paquet r�seau
* Structures de donn�es et algorithmes - DUT1 Paris Descartes
*/

/**
* @brief Structure de donn�es de type PqReseau
*/
#include "PaquetReseau.h"
#include "Liste2.h"
struct  MessageEnCours{
	enum { MAX = 21 };
	IdMessage IdMess;
	Liste2 listeB;
	unsigned int LgMes;
	unsigned int nbPRecus;
	unsigned int LastPRecu;
};

/** Initialiser et allouer en m�moire dynamique
la variable m de type MessageEnCours */
void initialiser_mc(MessageEnCours& mc, const PaquetReseau& pr);

/** D�sallouer la variable m de type MessageEnCours */
void detruire_mc(MessageEnCours& mc);

#endif